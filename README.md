# Tank You

Simple tank shooting game created with Unity during Basics of game development-course in 2018. We had a group of four people and my task in the project was mostly programming health-/forcebars and some shooting and movement logic.
